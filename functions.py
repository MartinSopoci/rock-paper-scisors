from collections import Counter

import numpy as np
import pandas as pd


def load_df(file_name: str) -> pd.DataFrame:
    return pd.read_csv(str(file_name), keep_default_na=False, encoding="utf-8-sig")


def ask_yn_question(question: str) -> bool:
    while True:
        response = input(f"{question} [y/n]: ")
        if (response.lower() == 'y') or (response == ''):
            return True
        elif response.lower() == 'n':
            return False
        else:
            print('Something other than: "y" or "n" was given.\n')


def get_new_session_id() -> str:
    df_history = load_df('history.csv')
    if np.isnan(df_history[['session_id']].max()[0]):
        return "1"
    else:
        return str(int(df_history[['session_id']].max()[0]) + 1)


def get_total_player_playes() -> list:
    df_history = load_df('history.csv')
    df_history.sort_values(by=['timestamp'])

    return df_history['player_turn'].values


def get_total_player_games() -> list:
    df_history = load_df('history.csv')
    df_history.sort_values(by=['timestamp'])

    return df_history['session_id'].unique()


def get_specific_player_games(id: int) -> list:
    df_history = load_df('history.csv')
    ids = df_history['session_id'].unique()

    if id in ids:
        df_history.sort_values(by=['timestamp'])
        return df_history['player_turn'][df_history['session_id'] == id].values
    else:
        return []


# one hot encoder
def one_hot(data):
    result = []
    for i in data:
        if i == 1:
            result.append([1, 0, 0])
        elif i == 2:
            result.append([0, 1, 0])
        elif i == 3:
            result.append([0, 0, 1])
        else:
            print(f'Unrecognized move!!! {i}')
            break

    return np.array(result)


def get_most_used(play: list) -> int:
    return Counter(play).most_common(1)[0][0]
