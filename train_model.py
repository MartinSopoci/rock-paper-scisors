import keras
import numpy as np
import tensorflow as tf
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.models import Sequential

from config.config import TIMESTEP_LENGTH, INPUT_DIM, OUTPUT_DIM, HIDDEN_UNITS, DROPOUT, EPOCHS, BATCH_SIZE, \
    MODEL_FILE_PATH
from functions import get_total_player_playes, one_hot


# reshape values to metch RNN input
def reshape_data(data, look_back):
    X, Y = [], []
    for i in range(len(data) - look_back):
        X.append(data[i:(i + look_back), :])
        Y.append(data[i + look_back, :])
    return np.array(X), np.array(Y)


def create_train_model(train_X, train_Y, timestep_length, hidden_units, dropout, epochs, batch_size):
    model = Sequential()
    model.add(LSTM(hidden_units, input_shape=(timestep_length, INPUT_DIM)))
    model.add(Dropout(dropout))  # stack LSTM architecture;  does not do much in this case
    model.add(Dense(OUTPUT_DIM, activation='softmax'))  # the number of unit needs to match the output dim size

    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

    model.fit(train_X, train_Y, epochs=epochs, batch_size=batch_size, verbose=0)

    return model


def train_model():
    print('Training the model for RNN ...')

    data = get_total_player_playes()
    hot_data = one_hot(data)
    X_train, Y_train = reshape_data(hot_data, TIMESTEP_LENGTH)

    model = create_train_model(X_train, Y_train, timestep_length=TIMESTEP_LENGTH,
                               hidden_units=HIDDEN_UNITS, dropout=DROPOUT,
                               epochs=EPOCHS, batch_size=BATCH_SIZE)

    model.save(MODEL_FILE_PATH)

    print('... Done training the model for RNN')


def retrain_model():
    print('Retraining the model for RNN ...')

    data = get_total_player_playes()
    hot_data = one_hot(data)
    X_train, Y_train = reshape_data(hot_data, TIMESTEP_LENGTH)

    physical_devices = tf.config.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

    model = keras.models.load_model(MODEL_FILE_PATH)
    model.fit(X_train, Y_train, epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=0)
    model.save(MODEL_FILE_PATH)

    print('... Done retraining the model for RNN')
