import datetime
import json
import sys
from typing import Tuple

from strategies.combined_strategy import get_bot_response


def convert_keys_to_int(input_dict: dict) -> dict:
    try:
        return {int(k): v for k, v in input_dict.items()}
    except ValueError:
        print('IDs in the rules were not all integers.')
        sys.exit()


class RpsBot:
    def __init__(self, play_id: str):
        self.play_id = play_id
        self.total_score = {"bot": 0,
                            "player": 0,
                            "tie": 0,
                            "total": 0
                            }

        with open("config/rules.json", "r", encoding="utf8") as rules_file:
            self.rules = convert_keys_to_int(json.load(rules_file))

    def give_rules(self):
        message = "#################### RULES ###################\n\n" \
                  "Rules of the game are following:\n" \
                  "You can play:\n\n"

        for key in self.rules.keys():
            accepts = '", "'.join(self.rules[key]['accept'])
            message = message + f'{key}) {self.rules[key]["name"]} \nfor which input can be: "{accepts}"\n'
            message = message + f'{self.rules[key]["name"]} destroy {self.rules[self.rules[key]["kills"]]["name"]}\n'
            message = message + f'{self.rules[self.rules[key]["dies"]]["name"]} destroy {self.rules[key]["name"]}\n\n'

        message = message + 'If you wish to play again just enter another valid response. \n' \
                            'To end or leave write "q" or "quit".\n' \
                            '###############################################\n'

        print(message)

    def verify_response(self, response: str) -> int:
        for key in self.rules.keys():
            if response in self.rules[key]['accept']:
                return key

        return -1

    def request_response(self) -> int:
        while True:
            response = input(f"What is your play?\n")
            if response == 'help':
                self.give_rules()
            elif (response == 'q') or (response == 'quit'):
                self.get_total_score()
                return -1
            else:
                player_play = self.verify_response(response)
                if player_play == -1:
                    print('#################### ERROR ####################\n\n'
                          'Response was not recognized.\n'
                          'For rules write "help", to quit write "q" or "quit".\n'
                          '###############################################\n\n')
                else:
                    return player_play

    def get_result(self, bot: int, player) -> Tuple[str, str]:
        if self.rules[bot]['kills'] == player:
            return "bot", "I have won!!!"
        elif self.rules[bot]['dies'] == player:
            return "player", "You have won!!!"
        else:
            return "tie", "Its a tie!!!"

    def get_total_score(self):
        total_play = self.total_score['total'] - self.total_score['tie']

        if total_play <= 0:
            success_rate = 0
        else:
            success_rate = round(self.total_score['player'] / total_play, 4) * 100

        message = f"Thank you for playing!\n\n" \
                  f"In total:\n" \
                  f"\tyou have won: {self.total_score['player']} times\n" \
                  f"\tI have won: {self.total_score['bot']} times\n" \
                  f"\tit was a tie {self.total_score['tie']} times.\n\n" \
                  f"Your success is: {success_rate}%"

        print(message)

    def save_result(self, bot: int, player: int, result: str, bot_pred: int, strategy: str):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        with open("history.csv", "a") as file_object:
            file_object.write(f'"{self.play_id}",'
                              f'"{timestamp}",'
                              f'"{bot}",'
                              f'"{player}",'
                              f'"{result}",'
                              f'"{bot_pred}",'
                              f'"{strategy}"\n')

    def give_name(self, play: int) -> str:
        return self.rules[play]['name']

    def give_current_id(self) -> str:
        return self.play_id

    def play(self):
        player_play = self.request_response()

        if player_play != -1:
            bot_play, bot_prediction, strategy = get_bot_response(session_id=self.give_current_id(), rules=self.rules)
            result, message = self.get_result(bot_play, player_play)
            self.save_result(bot_play, player_play, result, bot_prediction, strategy)

            self.total_score['total'] += 1
            self.total_score[result] += 1

            response = f"You have played: {self.give_name(player_play)}\n" \
                       f"I have played: {self.give_name(bot_play)}\n" \
                       f"{message}\n"
            print(response)

            self.play()
