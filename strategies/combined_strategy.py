"""
Its using all the strategy. First when there are no playes it will use random strategy. After 100 plays it will use LTSM
model strategy.
"""
from typing import Tuple

from config.config import RATIO_STRATEGY_TRIGGER, RNN_STRATEGY_TRIGGER
from functions import get_total_player_playes
from strategies.play_ratio_strategy import get_bot_response as ratio_response
from strategies.random_strategy import get_bot_response as rnd_response
from strategies.rnn_strategy import get_bot_response as rnn_response


def get_bot_response(**kwargs) -> Tuple[int, int, str]:
    plays = len(get_total_player_playes())
    if 0 <= plays < RATIO_STRATEGY_TRIGGER:
        return rnd_response(), -1, 'random'

    elif RATIO_STRATEGY_TRIGGER <= plays < RNN_STRATEGY_TRIGGER:
        return ratio_response(session_id=kwargs['session_id'], rules=kwargs['rules'])

    elif RNN_STRATEGY_TRIGGER <= plays:
        return rnn_response(session_id=kwargs['session_id'], rules=kwargs['rules'])

    else:
        return -1, -1, 'error'
