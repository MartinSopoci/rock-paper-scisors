from typing import Tuple

import keras
import numpy as np
import tensorflow as tf
from pathlib import Path
from config.config import TIMESTEP_LENGTH, MODEL_FILE_PATH
from functions import get_total_player_playes, one_hot

physical_devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], True)


if Path(MODEL_FILE_PATH).is_file():
    model = keras.models.load_model(MODEL_FILE_PATH)


# reshape values to match RNN input
def reshape_data(data, look_back):
    return np.array([data[0:look_back, :]])


# converts that the max vlaue is 1  as thats the strategy he predicts
def convert_result(data):
    result = np.zeros(data.shape)
    maxpos = np.argmax(data, axis=1)

    for i, pos in enumerate(maxpos):
        result[i, pos] = 1
    result = result.astype(int)
    a = np.dot(result[0], [1, 2, 3])

    return a


def get_bot_response(**kwargs) -> Tuple[int, int, str]:
    previous_play = get_total_player_playes()[-TIMESTEP_LENGTH:]
    hot_pp = one_hot(previous_play)
    input_tensor = reshape_data(hot_pp, TIMESTEP_LENGTH)
    raw_prediction = model.predict(input_tensor)
    player_pred = convert_result(raw_prediction)

    return int(kwargs['rules'][player_pred]['dies']), int(player_pred), 'rnn'
