import random
"""
Playes random rock, paper or scissors.
"""

def get_bot_response(**kwargs) -> int:
    return random.randrange(1, 4, 1)
