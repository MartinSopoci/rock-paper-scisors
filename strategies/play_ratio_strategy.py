"""
If this is the first play it will return the opposite of what player played most last game.
"""
from typing import Tuple

from functions import get_specific_player_games, get_most_used


def get_bot_response(**kwargs) -> Tuple[int, int, str]:
    current_id = int(kwargs['session_id'])

    current_play_history = get_specific_player_games(current_id)
    if len(current_play_history) == 0:
        previous_play_history = get_specific_player_games(current_id - 1)

        player_pred = get_most_used(previous_play_history)

        return kwargs['rules'][player_pred]['dies'], player_pred, 'ratio'

    elif len(current_play_history) > 0:
        current_play_history = get_specific_player_games(current_id)

        player_pred = get_most_used(current_play_history)

        return kwargs['rules'][player_pred]['dies'], player_pred, 'ratio'

    else:
        print('Something went wrong in bot previous response.')
        return -1, -1, 'ratio'
