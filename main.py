import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from pathlib import Path
import sys
import math
from bot import RpsBot
from config.config import RNN_STRATEGY_TRIGGER, MODEL_FILE_PATH
from functions import get_new_session_id, ask_yn_question, get_total_player_playes
from train_model import train_model, retrain_model

if __name__ == "__main__":
    plays_before = len(get_total_player_playes())
    pb_ratio = math.floor(plays_before/RNN_STRATEGY_TRIGGER)

    # try:

    if ask_yn_question('Want to play the game of rock, paper, scissors? '):
        new_id = get_new_session_id()
        CURRENT_BOT = RpsBot(new_id)
        CURRENT_BOT.give_rules()
        CURRENT_BOT.play()

        new_id = int(new_id)
        plays_after = len(get_total_player_playes())
        pa_ratio = math.floor(plays_after/RNN_STRATEGY_TRIGGER)

        if Path(MODEL_FILE_PATH).is_file():
            if pa_ratio > pb_ratio:
                retrain_model()
        else:
            if plays_after >= RNN_STRATEGY_TRIGGER:
                train_model()


    # except:
    #     print("Unexpected error:", sys.exc_info()[0])

    print('Thank you.')

