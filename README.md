## Requirements

- numpy==1.19.4 
- pandas==1.1.5
- tensorflow-gpu==2.3.0
- keras==2.4.3

## How to use

Tu run the application run the main.py 

There are strategies in the strategies folder. In the bot.py on line 6 you can specify which strategy to use.

## Rules
You can play:

1) rock 
for which input can be: "r", "1", "rock"
rock destroy scissors
paper destroy rock

2) paper 
for which input can be: "p", "2", "paper"
paper destroy rock
scissors destroy paper

3) scissors 
for which input can be: "s", "3", "scissors"
scissors destroy paper
rock destroy scissors

If you wish to play again just enter another valid response. 
To end or leave write "q" or "quit".