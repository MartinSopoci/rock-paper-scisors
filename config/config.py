RATIO_STRATEGY_TRIGGER = 2  # number of playes to start using ratio strategy
RNN_STRATEGY_TRIGGER = 100  # number of games to start using RNN strategy

OUTPUT_DIM = 3
INPUT_DIM = 3

TIMESTEP_LENGTH = 15        # how many previous plays it takes into account
DROPOUT = 0.3               # dropout rate (1 means full drop out)
EPOCHS = 30                 # num of epochs (one epoch is one sweep of full training set)
HIDDEN_UNITS = 30           # size of the hidden units in each cell
BATCH_SIZE = 4              # num of training samples submitted for one fwd/bwd pass before weights are updated

MODEL_FILE_PATH = 'rnn_models/model1'